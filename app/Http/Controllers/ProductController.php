<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private function cgyData()
    {
        $cgies = \App\Models\Cgy::pluck('title', 'id');
        return ['cgies' => $cgies];
    }
    private function prepareSaveData(Request $request)
    {
        $data = $request->except('_token');
        if ($request->has('options')) {
            $data['options'] = implode(',', $data['options']);
        }
        ;

        if ($request->hasFile('pic')) { //判斷hasFile('pic')有沒有東西
            $file = $request->file('pic');
            if ($file->isValid()) { //判斷$file是否有效
                $extension = $file->getClientOriginalExtension();
                $filename = time() . "." . $extension;
                $data['pic'] = $filename;
                $file->move('D:\tools\xampp\htdocs\form\storage\app\public\images', $filename);
            }
        }
        return $data;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where('enabled', true)->get();
        return view('products.index', compact('products'));

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // $product = \App\Models\Cgy::pluck('title', 'id');
        $data = $this->cgyData();
        return view('products.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$data = $request->all(); //取得所有前台傳入的資料
        // $data = $request->except('_token'); //取得所有前台傳入的資料
        // if ($request->has('options')) {
        //     $data['options'] = implode(',', $data['options']);
        // }
        // if ($request->hasFile('pic')) {
        //     $file = $request->file('pic'); //獲取UploadFile例項
        //     if ($file->isValid()) { //判斷檔案是否有效
        //         //$filename = $file->getClientOriginalName(); //檔案原名稱
        //         $extension = $file->getClientOriginalExtension(); //副檔名
        //         $filename = time() . "." . $extension; //重新命名
        //         $data['pic'] = $filename;
        //         $file->move('D:\tools\xampp\htdocs\form\storage\app\public\images', $filename); //移動至指定目錄
        //     }
        // }
        // Product::create($data);
        // return redirect('products');
        $data = $this->prepareSaveData($request);
        Product::create($data);
        // dd($data);
        return redirect('products');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $data = $this->cgyData();
        $data['product'] = $product;
        return view('products.edit', $data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $data = $this->prepareSaveData($request);
        $product->update($data);
        return redirect('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->Delete();
        return redirect('products');
    }
}
