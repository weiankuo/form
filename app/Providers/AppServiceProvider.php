<?php

namespace App\Providers;

use App\Models\Product;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            $event->menu->add('表格', [
                'text' => '新增商品',
                'url' => 'products\create',
                'icon' => 'far fa-fw fa-file',
            ]);

            $event->menu->add([
                'text' => '產品管理',
                'url' => 'products',
                'icon' => 'far fa-fw fa-file',
                'label' => Product::count(),
                'label_color' => 'success',
            ]);

        });
    }
}
