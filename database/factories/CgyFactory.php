<?php

namespace Database\Factories;

use App\Models\Cgy;
use Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class CgyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cgy::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = Faker\Factory::create('zh_TW');
        return [
            'title' => $faker->name,
            'pic' => $faker->image('public\storage\images', 320, 320, 'cats', false),
            'desc' => $faker->realText(30),
            'enabled' => rand(0, 1),
            'sort' => rand(0, 9),
        ];

    }
}
