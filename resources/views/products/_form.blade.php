{{--  {!! Form::open(['url'=>'products','files'=>true]) !!}  --}}
{!! Form::label('title', '商品名稱', ['class'=>'awesome']) !!}
{!! Form::text('title',null, ['placeholder'=>'請輸入商品名稱']) !!}<br>
{!! Form::label('desc', '商品描述', ['class'=>'awesome2']) !!}
{!! Form::textarea('desc',null, ['class'=>'awesome2','col'=>50,'rows'=>10,'style'=>'resize:none;']) !!}<br>

{!! Form::hidden('source', 'mystore') !!}

{!! Form::label('enabled', '是否啟用') !!}
{!! Form::checkbox('enabled', 1, true) !!}<br>

{!! Form::label('options[]', '要包裝') !!}
{!! Form::checkbox('options[]','package') !!}
{!! Form::label('options[]', '要冷藏') !!}
{!! Form::checkbox('options[]','ice') !!}
{!! Form::label('options[]', '加贈品') !!}
{!! Form::checkbox('options[]','gift') !!}<br>

{{--  {!! Form::label('gender', '男性') !!}
{!! Form::radio('gender','male',false) !!}
{!! Form::label('gender', '女性') !!}
{!! Form::radio('gender','female',false) !!}<br>  --}}

{!! Form::label('price', '價格') !!}
{!! Form::number('price',null,['min'=>1,'max'=>10000]) !!}<br>

{{-- {!! Form::label('cgy_id', '分類') !!}
@php
    $list=[
'只買本體'=>['1'=>'pi 400(單賣)'],
'有加配件'=>['2'=>'pi 400 + 套件','3'=>'pi 400+套件+書籍']
    ];
@endphp
{!! Form::select('cgy_id',$list,null,['placeholder'=>'請選擇商品分類']) !!}<br> --}}

{{--有假資料時引入資料--}}
{!! Form::label('cgy_id', '分類') !!}
{!! Form::select('cgy_id',$cgies,['placeholder'=>'請選擇商品分類']) !!}<br>
{{--
{!! Form::label('month', '月份') !!}
{!! Form::selectMonth('month') !!} <br>

{!! Form::label('numbers', '日期') !!}
{!! Form::selectRange('numbers', 1, 31) !!} <br>

{!! Form::label('password', '密碼') !!}
{!! Form::password('password', ['placeholder'=>'請輸入密碼','required']) !!} <br>

{!! Form::label('email', '信箱') !!}
{!! Form::email('email',null,['placeholder'=>'請輸入信箱','required']) !!} <br>  --}}

{!! Form::label('上架日期', '請選擇上架日期') !!}
{!! Form::date('sell_at', \Carbon\Carbon::now()) !!}<br>

{!! Form::label('pic', '上傳照片') !!}
{!! Form::file('pic') !!}<br>

{!! Form::submit('送出表單') !!}
{!! Form::reset('清空表單') !!}
{{--  {!! Form::close() !!}  --}}
