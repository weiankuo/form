
@extends('adminlte::page')

@section('title', '智能中心')

@section('content_header')
    <h1>詳細資訊</h1>
@stop

@section('content')
<h3>分類:{{ $product->cgy->title}}</h3>
<h1>{{$product->title}}</h1>
<img src="{{ asset('storage/images/'.$product->pic) }}" alt="">
<h5 style="color:red;">{{ $product->price }}</h5>
<p>{{$product->desc}}</p>

備註:{{$product->options}} <br>
是否開放:{{$product->enabled}} <br>
何時上架:{{$product->sell_at}} <br>
來源:{{$product->source}}<br><br>

<a href="{{ url('products/'.$product->id.'/edit') }}">編輯資料</a>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
