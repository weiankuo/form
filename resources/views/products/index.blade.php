@extends('adminlte::page')

@section('title', '智能中心')

@section('content_header')
    <h1>產品管理</h1>
@stop

@section('content')
<table>
    <tr>
    <th>ID</th>
    <th>分類</th>
    <th>標題</th>
    <th>圖片</th>
    <th>價格</th>
</tr>
@foreach($products as $product)
{{--  products是productcontroller裡 function index中的函式  --}}
<tr>
    <td><a href="{{ url('products/'.$product->id) }}">{{$product->id}}</a></td>
    <td>{{ $product->cgy->title }}</td>
    {{--  cgy -> models Product裡Cgy函式指定的位置  --}}
    <td><a href="">{{ $product->title }}</a></td>
    <td><img src="{{ asset('storage/images/' . $product->pic) }}" alt=""></td>
    <td>{{ $product->price }}</td>
</tr>
@endforeach
    </table>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
@stop

