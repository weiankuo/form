@extends('adminlte::page')

@section('title', '智能中心')

@section('content_header')
    <h1>修改商品</h1>
@stop

@section('content')
{!! Form::open(['url'=>'products/'.$product->id,'method'=>'DELETE','style'=>'display:none;','id'=>'del-form']) !!}

{!! Form::close() !!}
<a href="#" onclick="event.preventDefault();document.getElementById('del-form').submit();">刪除商品</a>

{!! Form::model($product,['url'=>'products/'.$product->id,'files'=>true,'method'=>'put']) !!}

@include('products._form',['isDelete'=>true])
{!! Form::close() !!}
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
