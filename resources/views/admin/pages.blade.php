@extends('adminlte::page')

@section('title', '智能中心')

@section('content_header')
    <h1>頁面管理</h1>
@stop

@section('content')
{!! Form::open(['url'=>'products','files'=>true]) !!}
@include('products._form')
{!! Form::close() !!}
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
